export ANTIGEN=~/.dotfiles/antigen

export PATH="~/bin:/usr/local/bin:/usr/bin:/bin:/usr/bin/X11:/usr/games:/usr/lib/mit/bin:$HOME/Code/dart-sdk/bin:$HOME/.cabal/bin"

source $ANTIGEN/antigen.zsh

# Load the oh-my-zsh's library.
antigen use oh-my-zsh

# Bundles from the default repo (robbyrussell's oh-my-zsh).
antigen bundles <<EOBUNDLES
	git
	vundle
	pip
	vagrant
	command-not-found
	zsh-users/zsh-syntax-highlighting
EOBUNDLES

# Syntax highlighting bundle.

# Load the theme.
antigen theme robbyrussell

# Tell antigen that you're done.
antigen apply

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

alias csi="rlwrap csi"

alias matrix='LC_ALL=C tr -c "[:digit:]" " " < /dev/urandom | dd cbs=$COLUMNS conv=unblock | GREP_COLOR="1;32" grep --color "[^ ]"'
