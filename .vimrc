set nocompatible              " be iMproved, required
set number
set expandtab
set tabstop=4
set shiftwidth=4
set autoindent
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim

call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

Plugin 'tpope/vim-surround'
Plugin 'tpope/vim-dispatch'
Plugin 'tpope/vim-fugitive'

Plugin 'scrooloose/syntastic'
Plugin 'scrooloose/nerdcommenter'
Plugin 'scrooloose/nerdtree'
Plugin 'scrooloose/vim-statline'

Plugin 'ryanss/vim-hackernews'

Plugin 'dart-lang/dart-vim-plugin'
Plugin 'haruyama/scheme.vim'

Plugin 'flazz/vim-colorschemes'

call vundle#end()            " required

filetype plugin indent on    " required
syntax on

colorscheme twilight256

map <C-n> :NERDTreeToggle<CR>
